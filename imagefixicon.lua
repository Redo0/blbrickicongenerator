--love 1

local lg = love.graphics
local li = love.image

local function round(x)
	return math.floor(x+0.5)
end

local function clamp(x, a, b)
	return math.min(math.max(x, a), b)
end

local function imageFixIcon(imagedata, identifier, brickmode)
	local xs, ys = imagedata:getDimensions()
	
	--remove upper skybox bullshit
	if brickmode then
		for x = 1, xs do
			for y = 1, ys/5 do
				imagedata:setPixel(x-1, y-1, 1, 0, 1, 1)
			end
		end
	end
	
	local xmin = xs
	local xmax = 1
	local ymin = ys
	local ymax = 1
	
	for x = 1, xs do
		for y = 1, ys do
			local r, g, b, a = imagedata:getPixel(x-1, y-1)
			
			if r==1 and g==0 and b==1 then
				imagedata:setPixel(x-1, y-1, 1, 1, 1, 0)
			else
				if x<xmin then xmin = x end
				if x>xmax then xmax = x end
				if y<ymin then ymin = y end
				if y>ymax then ymax = y end
				
				local l = (r+g+b)/3
				
				imagedata:setPixel(x-1, y-1, l, l, l, a)
			end
		end
	end
	
	local xc = round((xmax-xmin)/2+xmin)
	local yc = round((ymax-ymin)/2+ymin)
	
	local xextent = math.max(xmax-xmin, 96)
	local yextent = math.max(ymax-ymin, 96)
	
	local scale = 1
	if xextent>96 or yextent>96 then
		scale = 96/math.max(xextent, yextent)
	end
	
	--[[
	local newimagedata = li.newImageData(96, 96)
	for x = 1, 96 do
		for y = 1, 96 do
			local xp = math.round(xc+x/scale-48/scale), 0, xs-1
			local yp = math.round(yc+y/scale-48/scale), 0, ys-1
			
			local r, g, b, a
			if xp>=0 and xp<=xs-1 and yp>=0 and yp<=ys-1 then
				r, g, b, a = imagedata:getPixel(xp, yp)
			else
				r, g, b, a = 1, 1, 1, 0
			end
			
			local l = (r+g+b)/3
			
			newimagedata:setPixel(x-1, y-1, l, l, l, a)
		end
	end]]
	
	--scale = 1
	if not brickmode then
		scale = scale*0.95
	end
	
	local drawable = lg.newImage(imagedata)
	
	local canvas = lg.newCanvas(96, 96)
	
	lg.setCanvas(canvas) lg.push()
		lg.clear()
		local v = 1
		if brickmode then v = 0.95 end
		lg.setColor(v,v,v,1)
		local filter = "linear"
		--local filter = "nearest"
		lg.setDefaultFilter(filter, filter, 2)
		lg.draw(drawable, 48, 48, 0, scale, scale, xc, yc)
	lg.pop() lg.setCanvas()
	
	local newimagedata = canvas:newImageData()
	
	if not brickmode then
		newimagedata:setPixel(identifier%96, math.floor(identifier/96), 1, 1, 1, 1)
	end
	
	return newimagedata
end

return imageFixIcon
