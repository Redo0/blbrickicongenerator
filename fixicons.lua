--love 1

local le = love.event
local lg = love.graphics
local li = love.image
local lf = love.filesystem

--------------------------------------------------------------

local drawable
local function yieldImage(imagedata)
	drawable = lg.newImage(imagedata)
	coroutine.yield()
end

--------------------------------------------------------------

local function imageLoad(filename)
	local imagedata = li.newImageData(filename..".png")
	return imagedata
end

local function imageStore(imagedata, filename)
	local filedata = imagedata:encode("png")
	
	local outfile = io.open(filename..".png", "wb")
	outfile:write(filedata:getString())
	outfile:close()
end

--------------------------------------------------------------

local imageFixIcon = require "imagefixicon"

--------------------------------------------------------------

local function fixAllIcons()
	local files = lf.getDirectoryItems("icons-unfixed")
	for fileidx, filename in ipairs(files) do
		local fileext = filename:match("%.[a-zA-Z0-9]+$")
		if fileext==".png" then
			local filepart = filename:gsub("%.[a-zA-Z0-9]+$", "")
			
			local imagedata = imageLoad("icons-unfixed/"..filepart)
			print(filepart)
			
			local newimagedata = imageFixIcon(imagedata, fileidx, true)
			
			imageStore(newimagedata, "icons-fixed/"..filepart)
			
			yieldImage(newimagedata)
		end
	end
	print("Done")
end

--------------------------------------------------------------

local cor
function love.load()
	cor = coroutine.create(fixAllIcons)
	--fixAllIcons()
end

function love.draw()
	if drawable then
		lg.draw(drawable)
	end
end

function love.update(dt)
	coroutine.resume(cor)
end

function love.keypressed(k)
	if k=="escape" then le.quit() end
end
